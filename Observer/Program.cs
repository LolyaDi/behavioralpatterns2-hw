﻿using System;
using System.Collections.Generic;

namespace Observer
{
    public interface IProduct
    {
        // Присоединяет наблюдателя к производителю.
        void Attach(IShop shop);

        // Отсоединяет наблюдателя от производителя.
        void Detach(IShop shop);

        // Уведомляет всех наблюдателей о событии.
        void Notify();
    }

    public abstract class Cocktail : IProduct
    {
        // Список заведений(наблюдателей).
        private List<IShop> _shops = new List<IShop>();
        private double _price;

        public Cocktail(double price)
        {
            _price = price;
        }
        
        public void Attach(IShop shop)
        {
            _shops.Add(shop);
        }

        public void Detach(IShop shop)
        {
            _shops.Remove(shop);
        }

        public void Notify()
        {
            foreach (IShop shop in _shops)
            {
                shop.Update(this);
            }
        }

        public double Price
        {
            get { return _price; }
            set
            {
                if (_price != value)
                {
                    _price = value;
                    
                    // Автоматически присылает уведомление об изменениях прайса 
                    Notify(); 
                }
            }
        }
    }

    public class Mohito : Cocktail
    {
        public Mohito(double price) : base(price) { }
    }

    public interface IShop
    {
        // Получает обновление от производителя
        void Update(Cocktail cocktail);
    }

    // Конкретные Наблюдатели реагируют на обновления, выпущенные Производителем, к
    // которому они прикреплены.
    public class Shop : IShop
    {
        private string _name;

        public Shop(string name)
        {
            _name = name;
        }

        public void Update(Cocktail cocktail)
        {
            Console.WriteLine("Notified '{0}' of {1}'s " + "price change to {2}$.", _name, cocktail.GetType().Name, cocktail.Price);
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var mohito = new Mohito(0.82);
            mohito.Attach(new Shop("Mackay's"));
            mohito.Attach(new Shop("Johnny's"));

            // При изменении прайса коктейля наблюдатели будут осведомлены.
            mohito.Price = 10.79;
            
            Console.ReadLine();
        }
    }
}
